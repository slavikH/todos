import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from './store/state/app.state';
import { GetTodos, AddTodo, DeleteTodo, UpdateTodo } from './store/actions/todo.action';
import { selectTodosList } from './store/selectors/todo.selector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public title = 'todos';
  public addItemForm: FormGroup;
  public todos$: Observable<any>
  public input: boolean;

  constructor(private store: Store<AppState>) {
    this.input = false;
    this.todos$ = this.store.pipe(select(selectTodosList))
  }

  ngOnInit() {
    this.addItemForm = new FormGroup({
      myinput: new FormControl(null, Validators.required)
    })
    this.store.dispatch(new GetTodos())
  }

  public onDelete(todo) {
    this.store.dispatch(new DeleteTodo(todo));
  }

  public onAddBtn() {
    const value = this.addItemForm.get('myinput').value;
    this.addItemForm.reset();
    this.store.dispatch(new AddTodo(value));
  }

  public onUpdate(todo, text) {
    this.store.dispatch(new UpdateTodo(todo, text));
  }

}
