import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database'

import { appReducer } from '../app/store/reducers/app.reducer';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { TodoService } from './services/todo.service';
import { TodoEffects } from './store/effects/todo.effect';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    StoreModule.forRoot(appReducer),
    EffectsModule.forRoot([TodoEffects]),
    ReactiveFormsModule
  ],
  providers: [
    TodoService,
    AngularFireDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
