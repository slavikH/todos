import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { map } from 'rxjs/operators';

@Injectable()
export class TodoService {
    public text: any
    constructor(private af: AngularFireDatabase) { }
    public getTodos() {
        return this.af.list('/lists').snapshotChanges()
            .pipe(
                map(changes =>
                    changes.map(c => {
                        const value = c.payload.val()
                        if (typeof value == 'object') {
                            this.text = value;
                            return {
                                key: c.payload.key,
                                value: this.text.value
                            }
                        }
                        else {
                            this.text = c.payload.val()
                            return {
                                key: c.payload.key,
                                value: this.text
                            }
                        }
                    })
                )
            );
    }

    public addTodo(todo) {
        return this.af.list('/lists').push(todo)
    }

    public deleteTodo(todo) {
        return this.af.list(`/lists`).snapshotChanges()
            .pipe(
                map(changes =>
                    changes.map(c => {
                        const value = c.payload.val()
                        if (typeof value == 'object') {
                            this.text = value;
                            return {
                                key: c.payload.key,
                                value: this.text.value
                            }
                        }
                        else {
                            this.text = c.payload.val()
                            return {
                                key: c.payload.key,
                                value: this.text
                            }
                        }
                    })
                ),
                map(response => {
                    response.forEach(element => {
                        if (element.value == todo.value) {
                            return this.af.object('/lists/' + element.key).remove()
                        }
                    });
                })
            )
    }

    public updateTodo(text, todo) {
        return this.af.list(`/lists`).snapshotChanges()
            .pipe(
                map(changes =>
                    changes.map(c => {
                        return {
                            key: c.payload.key,
                            value: c.payload.val()
                        }
                    })
                ),
                map(response => {
                    response.forEach(element => {
                        if (text !== todo.value && todo.key == element.key) {
                            return this.af.list('/lists').update(element.key, { value: text })
                        }
                    });
                })
            )
    }
}