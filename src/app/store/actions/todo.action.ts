import { Action } from '@ngrx/store';

export enum ITodoActions {
    GetTodos = '[App] Get todos',
    GetTodosSuccess = '[App] Get todos success',
    AddTodo = '[App] Add todo',
    AddTodoSuccess = '[App] Add todo success',
    AddTodoFail = '[App] Add todo fail',
    DeleteTodo = '[App] Delete todo',
    DeleteTodoSuccess = '[App] Delete todo success',
    DeleteTodoFail = '[App] Delete todo fail',
    UpdateTodo = '[App] Update todo',
    UpdateTodoSuccess = '[App] Update todo success',
    UpdateTodoFail = '[App] Update todo fail'
}

export class GetTodos implements Action {
    readonly type = ITodoActions.GetTodos;
}

export class GetTodosSuccess implements Action {
    readonly type = ITodoActions.GetTodosSuccess;
    constructor(public payload: any) { }
}

export class AddTodo implements Action {
    readonly type = ITodoActions.AddTodo;
    constructor(public payload: string) { }
}

export class AddTodoSuccess implements Action {
    readonly type = ITodoActions.AddTodoSuccess;
    constructor(public payload: string) { }
}

export class AddTodoFail implements Action {
    readonly type = ITodoActions.AddTodoFail;
    constructor(public payload: string) { }
}

export class DeleteTodo implements Action {
    readonly type = ITodoActions.DeleteTodo;
    constructor(public payload: string) { }
}


export class DeleteTodoSuccess implements Action {
    readonly type = ITodoActions.DeleteTodoSuccess;
    constructor(public payload: string) { }
}

export class DeleteTodoFail implements Action {
    readonly type = ITodoActions.DeleteTodoFail;
    constructor(public payload: string) { }
}

export class UpdateTodo implements Action {
    readonly type = ITodoActions.UpdateTodo;
    constructor(public payload: string, public todo: any) { }
}

export class UpdateTodoSuccess implements Action {
    readonly type = ITodoActions.UpdateTodoSuccess;
    constructor(public payload: string) { }
}

export class UpdateTodoFail implements Action {
    readonly type = ITodoActions.UpdateTodoFail;
    constructor(public payload: string, public error: any) { }
}

export type TodoActions =
    GetTodos |
    GetTodosSuccess |
    AddTodo |
    AddTodoFail |
    AddTodoSuccess |
    DeleteTodo |
    DeleteTodoFail |
    DeleteTodoSuccess |
    UpdateTodo |
    UpdateTodoFail |
    UpdateTodoSuccess;