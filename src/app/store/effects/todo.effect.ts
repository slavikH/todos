import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { switchMap, tap, catchError } from 'rxjs/operators';
import {
    GetTodos,
    GetTodosSuccess,
    AddTodo,
    AddTodoFail,
    AddTodoSuccess,
    ITodoActions,
    DeleteTodo,
    DeleteTodoSuccess,
    DeleteTodoFail,
    UpdateTodo,
    UpdateTodoSuccess,
    UpdateTodoFail
} from '../actions/todo.action';
import { TodoService } from 'src/app/services/todo.service';
import { of } from 'rxjs';

@Injectable()
export class TodoEffects {
    constructor(
        private _actions: Actions,
        private todoService: TodoService
    ) { }
    @Effect()
    getTodos$ = this._actions.pipe(
        ofType<GetTodos>(ITodoActions.GetTodos),
        switchMap(() => this.todoService.getTodos()),
        switchMap(response => of(new GetTodosSuccess(response))),
    );

    @Effect()
    addTodo$ = this._actions.pipe(
        ofType<AddTodo>(ITodoActions.AddTodo),
        switchMap((action) => this.todoService.addTodo(action.payload)),
        switchMap(() => of(new AddTodoSuccess('Todo successfully added'))),
        catchError(() => of(new AddTodoFail('Failed to add todo'))),
    )

    @Effect()
    deleteTodo$ = this._actions.pipe(
        ofType<DeleteTodo>(ITodoActions.DeleteTodo),
        switchMap((action) => this.todoService.deleteTodo(action.payload)),
        switchMap(() => of(new DeleteTodoSuccess('Successfully deleted'))),
        catchError(() => of(new DeleteTodoFail('Failed to delete todo'))),
    )

    @Effect()
    updateTodo$ = this._actions.pipe(
        ofType<UpdateTodo>(ITodoActions.UpdateTodo),
        switchMap((action) => this.todoService.updateTodo(action.todo, action.payload)),
        switchMap(() => of(new UpdateTodoSuccess('Successfully updated'))),
        catchError((error) => of(new UpdateTodoFail('Failed to update todo', error))),
        tap(res => console.log(res))
    )
}