import { ActionReducerMap } from '@ngrx/store';
import { AppState } from '../state/app.state';
import { todoReducer } from './todo.reducer';

export const appReducer: ActionReducerMap<AppState> = {
    todos: todoReducer
}