import { TodoState, initialTodoState } from '../state/todo.state';
import { TodoActions, ITodoActions } from '../actions/todo.action';

export const todoReducer = (
    state = initialTodoState,
    action: TodoActions
): TodoState => {
    switch (action.type) {
        case ITodoActions.GetTodosSuccess: {
            return {
                ...state,
                todos: action.payload
            }
        }

        case ITodoActions.AddTodo: {
            return {
                ...state,
                todo: action.payload
            }
        }

        case ITodoActions.AddTodoSuccess: {
            return {
                ...state,
                todo: action.payload
            }
        }

        case ITodoActions.AddTodoFail: {
            return {
                ...state,
                todo: action.payload
            }
        }

        case ITodoActions.DeleteTodo: {
            return {
                ...state,
                todo: action.payload
            }
        }

        case ITodoActions.DeleteTodoSuccess: {
            return {
                ...state,
                todo: action.payload
            }
        }

        case ITodoActions.DeleteTodoFail: {
            return {
                ...state,
                todo: action.payload
            }
        }

        case ITodoActions.UpdateTodo: {
            return {
                ...state,
                todo: { text: action.payload, todo: action.todo }
            }
        }

        case ITodoActions.UpdateTodoSuccess: {
            return {
                ...state,
                todo: action.payload
            }
        }


        case ITodoActions.UpdateTodoFail: {
            return {
                ...state,
                todo: action.payload
            }
        }


        default:
            return state
    }
}