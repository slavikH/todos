import { createSelector } from '@ngrx/store';
import { AppState } from '../state/app.state';
import { TodoState } from '../state/todo.state';

const selectTodos = (state: AppState) => state.todos;

export const selectTodosList = createSelector(
    selectTodos,
    (state: TodoState) => state.todos
)