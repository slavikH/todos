import { TodoState, initialTodoState } from './todo.state';

export interface AppState {
    todos: TodoState
}

export const initialAppState = {
    todos: initialTodoState
}

export function getInitialState() {
    return initialAppState
}