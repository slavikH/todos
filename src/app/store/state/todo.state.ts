export interface Todos {
    todos: any;
    todo?: any
}

export interface TodoState {
    todos: Todos
    todo?: any
}

export const initialTodoState: TodoState = {
    todos: null
}